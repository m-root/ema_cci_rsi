import dateutil.parser as dp
import ccxt


def candles():#'BTC/USD', 'ETH/USD'

    bitmex = ccxt.bitmex()

    # params:
    symbol = 'BTC/USD'
    timeframe = '5m'
    limit = 100
    params = {'partial': False}
    since = bitmex.milliseconds() - limit * 60 * 4000
    candles = bitmex.fetch_ohlcv(symbol, timeframe, since, limit, params)





    cand = []
    g = []

    for f in candles:
        if float(dp.parse(bitmex.iso8601(f[0])).strftime("%M")) % 15 != 0:
            g.append([bitmex.iso8601(f[0])] + f[1:])

        if float(dp.parse(bitmex.iso8601(f[0])).strftime("%M")) % 15 == 0:
            g.append([bitmex.iso8601(f[0])] + f[1:])
            if len(g) == 1:
                f = [bitmex.iso8601(f[0])] + f[1:]
                cand.append(f)
                del g[:]

            elif len(g) == 2:
                f = [g[1][0], g[0][1], max(g[0][2], g[1][2]), min(g[0][3], g[1][3]), g[1][4], sum([g[0][-1], g[1][-1]])]
                cand.append(f)
                del g[:]

            elif len(g) == 3:

                f = [g[2][0], # 0
                     g[0][1], #Open 1
                     max(g[0][2], g[1][2], g[2][2]), #High 2
                     min(g[0][3], g[1][3], g[2][3]), #Low 3
                     g[2][4], # Close 4
                     sum([g[0][-1], g[1][-1], g[2][-1]])] #Volume 5

                cand.append(f)
                del g[:]
    # OHLC
    return [
        # len([f[0] for f in cand]),
        # [f[0] for f in cand],
        [f[1] for f in cand],
        [f[2] for f in cand],
        [f[3] for f in cand],
        [f[4] for f in cand]
    ]


#######################################################
################      Sanity Tester     ###############
#######################################################
# while True:
#     print()
#     print(candles())
#     print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
#     for d in candles():
#         print(d)
#     import time
#     time.sleep(2)
